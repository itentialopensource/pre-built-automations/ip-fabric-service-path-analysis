
## 0.0.12 [08-29-2024]

deprecate the Pre-Built

See merge request itentialopensource/pre-built-automations/ip-fabric-service-path-analysis!6

2024-08-29 23:08:24 +0000

---

## 0.0.11 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/ip-fabric-service-path-analysis!5

---

## 0.0.10 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/ip-fabric-service-path-analysis!4

---

## 0.0.9 [12-21-2021]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/ip-fabric-service-path-analysis!2

---

## 0.0.8 [09-22-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/ip-fabric-service-path-analysis!1

---

## 0.0.7 [09-21-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/ip-fabric-service-path-analysis!1

---

## 0.0.6 [09-21-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/ip-fabric-service-path-analysis!1

---

## 0.0.5 [09-07-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.4 [09-07-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.3 [09-07-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.2 [09-07-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
